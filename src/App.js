import React from 'react';
import logo from './logo.svg';
import './App.css';

class App extends React.Component {
  handleJoinRoom = () => {
    console.log('joining room');
  }
  handleCreateRoom = () => {
    console.log('creating room');
  }

  render() {
    return (
      <div className="App">
      <Navbar></Navbar>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <div className="field is-horizontal">
          <div className="field-body">
            <form name="join" method="get" action="/join">
              <div className="field has-addons">
                <div className="control">
                  <input type="text" name="roomId" placeholder="Room Name" className="input" />
                </div>
                <div className="control">
                  <input type="submit" value="Join Room" onClick={this.handleJoinRoom} className="button is-primary is-normal is-rounded" />
                </div>
              </div>
            </form>
          </div>
        </div>

        {/* Create Room button */}
        <div className="field is-horizontal">
          <div className="field-body">
            <form name="auth" method="get" action="/auth/spotify">
              <div className="field">
                <input type="submit" value="Login and Create Room" onClick={this.handleCreateRoom} className="button is-info is-normal is-rounded" />
              </div>
            </form>
          </div>
        </div>
        
      </header>
    </div>
    );
  }
}

// function App() {
//   return (
    
//   );
// }

function Navbar() {
  return (
    <div>
      <nav className="navbar is-fixed-top is-success">
        <div className="navbar-brand">
          <a className="navbar-item" href="/">
            <img src="/favicon.ico" alt="Some kind of icon"></img>
          </a>
          <a className="navbar-burger burger" role="button" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true" />
          </a>
        </div>
        <div id="navbarBasicExample" className="navbar-menu">
          <div className="navbar-start">
            <a href="/" className="navbar-item">Home</a>
          </div>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-primary">
                  <strong>Join Room</strong>
                </a>
                <a href="/auth/spotify" className="button is-light">Log In</a>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default App;
